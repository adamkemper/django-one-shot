from django.shortcuts import render
from todos.models import TodoList

# Create your views here.
def todo_list_view(request):
    todo_lists = TodoList.objects.all()
    
    return render(request, 'todos/todo_list.html', {'todo_lists': todo_lists})
